#!/usr/bin/python3

import os

from qgis.core import (
    Qgis,
    QgsApplication,
    QgsCoordinateReferenceSystem,
    QgsDataSourceUri,
    QgsProject,
    QgsRectangle,
    QgsSettings,
    QgsTask,
    QgsVectorLayer,
    QgsWkbTypes,
)
from qgis.gui import QgsExtentWidget
from qgis.PyQt import uic
from qgis.PyQt.QtGui import QPalette
from qgis.PyQt.QtWidgets import (
    QComboBox,
    QLabel,
    QStyle,
    QStyleOptionComboBox,
    QStylePainter,
)

REF_CLASS, BASE_CLASS = uic.loadUiType(
    os.path.join(os.path.dirname(__file__), "main_window.ui")
)


class QCustomComboBox(QComboBox):
    def __init__(self, parent) -> None:
        super(QCustomComboBox, self).__init__(parent)
        self._init_text: str = ""

    @property
    def init_text(self) -> str:
        return self._init_text

    @init_text.setter
    def init_text(self, text: str) -> None:
        self._init_text = text

    def paintEvent(self, paint_event) -> None:
        if self.currentText():
            return super(QCustomComboBox, self).paintEvent(paint_event)

        painter: QStylePainter = QStylePainter(self)
        p = self.palette()
        painter.setPen(p.color(QPalette.Text))
        # draw the combobox frame, focusrect and selected etc.
        opt: QStyleOptionComboBox = QStyleOptionComboBox()
        self.initStyleOption(opt)
        opt.currentText = self._init_text
        # set font italic
        font = painter.font()
        font.setItalic(True)
        painter.setFont(font)

        painter.drawComplexControl(QStyle.CC_ComboBox, opt)

        # draw the icon and text
        painter.drawControl(QStyle.CE_ComboBoxLabel, opt)


class ExtractParcelsWindow(BASE_CLASS, REF_CLASS):
    def __init__(self, iface, parent=None) -> None:
        super(ExtractParcelsWindow, self).__init__(parent)
        self.setupUi(self)
        self.iface = iface
        self.extent_widget: QgsExtentWidget = QgsExtentWidget()
        self.extent_widget.setMapCanvas(self.iface.mapCanvas())
        extent_output_crs = QgsCoordinateReferenceSystem("EPSG:2154")
        self.extent_widget.setOutputCrs(extent_output_crs)
        self.extent_widget.setNullValueAllowed(True)
        self.extent_widget.clear()
        self.lb_dep = QLabel("Département")
        self.lb_com = QLabel("Commune")
        self.lb_section = QLabel("Section")
        self.cb_dep: QCustomComboBox = QCustomComboBox(self)
        self.cb_com: QCustomComboBox = QCustomComboBox(self)
        self.cb_section: QCustomComboBox = QCustomComboBox(self)
        for wdgt in [
            self.lb_dep,
            self.lb_com,
            self.lb_section,
            self.cb_dep,
            self.cb_com,
            self.cb_section,
            self.gb_sql_filter,
            self.le_sql_filter,
        ]:
            wdgt.setEnabled(False)

        # add widgets
        self.vl_main.insertWidget(0, self.extent_widget)
        self.lyt_attr.addRow(self.lb_dep, self.cb_dep)
        self.lyt_attr.addRow(self.lb_com, self.cb_com)
        self.lyt_attr.addRow(self.lb_section, self.cb_section)
        # initialize data
        self.init_combo_data()
        # signals
        self.extent_widget.toggleDialogVisibility.connect(lambda v: self.setVisible(v))
        self.bb_run_or_cancel.accepted.connect(self.run_and_extract)
        self.bb_run_or_cancel.rejected.connect(lambda: self.close())

    def init_combo_data(self) -> None:
        params = QgsSettings()
        params.beginGroup(f"/PostgreSQL/connections/")
        self.cb_database.addItems(params.childGroups())

        self.cb_dep.addItems([None, "67", "68"])
        self.cb_dep.setCurrentIndex(-1)
        self.cb_dep.init_text = "2 départements"

    @property
    def extent(self) -> QgsRectangle:
        return self.extent_widget.outputExtent()

    @property
    def departement(self) -> str:
        return self.cb_dep.currentText()

    @property
    def commune(self) -> str:
        return self.cb_com.currentText()

    @property
    def section(self) -> str:
        return self.cb_section.currentText()

    @property
    def sql_filter(self) -> str:
        return self.le_sql_filter.text()

    def load_parcels(
        self,
        task: QgsTask,
    ) -> QgsVectorLayer:
        task.setProgress(1)

        params: QgsSettings = QgsSettings()
        params.beginGroup(f"/PostgreSQL/connections/{self.cb_database.currentText()}")
        cfg: dict[str, None] = {}
        cfg["aDatabase"] = params.value("database")
        cfg["aHost"] = params.value("host")
        cfg["aPort"] = params.value("port")
        cfg["authConfigId"] = params.value("authcfg")
        cfg["aUsername"] = None
        cfg["aPassword"] = None

        xmin: float = self.extent.xMinimum()
        ymin: float = self.extent.yMinimum()
        xmax: float = self.extent.xMaximum()
        ymax: float = self.extent.yMaximum()

        uri: QgsDataSourceUri = QgsDataSourceUri()
        uri.setConnection(**cfg)
        filter: str = f"v_foncier_sig.geom && ST_MakeEnvelope({xmin}, {ymin}, {xmax}, {ymax}, 2154)"
        uri.setDataSource("qgis", "v_foncier_sig", "geom", filter)
        uri.setKeyColumn("gid")

        task.setProgress(2)
        vlayer: QgsVectorLayer = QgsVectorLayer(
            uri.uri(False), "v_foncier_sig", "postgres"
        )
        task.setProgress(10)

        if not vlayer.isValid():
            raise ValueError("Invalid Layer")

        geom_types = {
            getattr(QgsWkbTypes, i): i
            for i in dir(QgsWkbTypes)
            if isinstance(getattr(QgsWkbTypes, i), QgsWkbTypes.Type)
            and 0 < getattr(QgsWkbTypes, i) < 7
        }

        vmemlyr: QgsVectorLayer = QgsVectorLayer(
            f"{geom_types[vlayer.wkbType()]}?crs={vlayer.crs().authid()}",
            "parcelles extraction",
            "memory",
        )

        vmemlyr_pr = vmemlyr.dataProvider()
        attrs = vlayer.dataProvider().fields().toList()
        vmemlyr_pr.addAttributes(attrs)
        vmemlyr.updateFields()
        task.setProgress(75)
        vmemlyr_pr.addFeatures(vlayer.getFeatures())
        task.setProgress(100)
        return vmemlyr

    def workdone(self, exception, result=None) -> None:
        if exception:
            self.iface.messageBar().pushMessage(
                "Erreur dans l'extraction des parcelles", Qgis.Critical
            )
        else:
            QgsProject.instance().addMapLayer(result)
            self.iface.messageBar().pushMessage(
                "L'extraction a été ajoutée au canevas", Qgis.Success
            )

    def run_and_extract(self):
        self.load_parcels_task: QgsTask = QgsTask.fromFunction(
            "Extraction parcelles", self.load_parcels, on_finished=self.workdone
        )
        QgsApplication.taskManager().addTask(self.load_parcels_task)
        self.close()
