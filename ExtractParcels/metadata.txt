[general]
name=Extraction parcelles
description=Outil d'extraction de parcelles depuis la base de données cadastre
about=Handy tool to extract parcels from cadastre DB
version=1.2
qgisMinimumVersion=3.0
qgisMaximumVersion=3.99
author=Julien Monticolo
email=contact@conservatoire-sites-alsaciens.eu
icon=images/app.svg
experimental=False
deprecated=False
tracker=https://github.com/geograndest/qgis-extract-parcels/issues
repository=https://gitlab.com/cenalsace/qgis-extract-parcels
homepage=https://github.com/geograndest/qgis-extract-parcels/wiki
tags=extraction,cadastre,parcelle,conservatoire,cen
